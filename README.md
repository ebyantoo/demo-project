# SPRING BOOT


1. Go To Command Palatte > spring.initializr.maven-project > Create Project Spring 
2. Create File src/main/java/HelloController.java

```
package com.helloworld.demoproject;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@RequestMapping(value="/")
    public String index() {
        return "Sping Test";
    }
}
```

3. Running Project
4. Akses Project With Port 8080

# SPRING BOOT CONTAINERIZATION

1. Dockerfile 

```
FROM openjdk:13-alpine

VOLUME /tmp

ADD /target/*.jar demo-project-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["java","-jar","/demo-project-0.0.1-SNAPSHOT.jar"]

```
2. gitlab-ci.yaml

```
image: gitlab/dind
services:
  - docker:dind

variables:
  DOCKER_DRIVER: overlay
  USER_GITLAB: ebyantoo
  APP_NAME: demo-project
  REPO: demo-project

stages:
  - build
  - test
  - docker

maven-build:
  image: maven:3-jdk-8
  stage: build
  script: "mvn clean package -B"
  artifacts:
    paths:
      - target/*.jar

maven-test:
  image: maven:3-jdk-8
  stage: test
  script: "mvn test"
  artifacts:
    paths:
      - target/*.jar

docker-build:
  stage: docker
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t registry.gitlab.com/ebyantoo/demo-project .
    - docker push registry.gitlab.com/ebyantoo/demo-project

```



