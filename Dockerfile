FROM openjdk:13-alpine

VOLUME /tmp

ADD /target/*.jar demo-project-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["java","-jar","/demo-project-0.0.1-SNAPSHOT.jar"]

